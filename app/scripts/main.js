jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.open-modal').on('click', function (e) {
    $('.s-modal').fadeIn();
  });

  $('.s-modal__centered').on('click', function (e) {
    if (e.target.className === 's-modal__centered') {
      $('.s-modal').fadeOut();
    }
  });

  $('.s-modal__close').on('click', function (e) {
    $('.s-modal').fadeOut();
  });

  $('.s-header__mob').on('click', function (e) {
    e.preventDefault();

    $('.s-info').slideToggle('fast');
    $('.s-header__nav').slideToggle('fast');
    $('.s-header__column').slideToggle('fast');
  });

  $('.s-seo__link').on('click', function (e) {
    e.preventDefault();

    if ($(this).hasClass('s-seo__link_active')) {
      $(this).removeClass('s-seo__link_active');
      $(this).find('span').html('Читать дальше');
      $(this).prev().removeClass('s-seo__content_active');
    } else {
      $(this).addClass('s-seo__link_active');
      $(this).find('span').html('Скрыть текст');
      $(this).prev().addClass('s-seo__content_active');
    }
  });

  $('.s-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.s-tabs__header li').removeClass('current');
    $('.s-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  new Swiper('.s-why__cards', {
    navigation: {
      nextEl: '.s-why .swiper-button-next',
      prevEl: '.s-why .swiper-button-prev',
    },
    loop: true,
    effect: null,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2,
      },
    },
    on: {
      slideChange: function () {
        $('.s-why-card').fadeOut(500);
        setTimeout(function () {
          $('.s-why-card').fadeIn(500);
        }, 500);
      },
    },
  });
});
